import request from '@/utils/request'

export function fetchUsers(page, page_size, date_start, date_end, agent_id, origin, type) {
  return request({
    url: '/user',
    method: 'get',
    params: { page, page_size, date_start, date_end, agent_id, origin, type }
  })
}

export function fetchBanUsers(page, page_size, date_start, date_end, agent_id, username, realname) {
  return request({
    url: '/user_black_list',
    method: 'get',
    params: { page, page_size, date_start, date_end, agent_id, username, realname }
  })
}

export function fetchUsersLogin(uid, page, page_size, date_start, date_end) {
  return request({
    url: '/user/login_log',
    method: 'get',
    params: { uid, page, page_size, date_start, date_end }
  })
}

export function banUser(id, ban_desc) {
  return request({
    url: '/user/disable',
    method: 'post',
    params: { id, ban_desc }
  })
}

export function unblockUser(id) {
  return request({
    url: '/user/enable',
    method: 'post',
    params: { id }
  })
}

export function editUser(id, agent_id, phone, phone_code, email, realname, password) {
  return request({
    url: '/user/edit',
    method: 'post',
    params: { id, agent_id, phone, phone_code, email, realname, password }
  })
}
