import request from '@/utils/request'

export function fetchAdminLogs(page, page_size) {
  return request({
    url: '/admin_user_oplog',
    method: 'get',
    params: { page, page_size }
  })
}

export function fetchAgentLogs(page, page_size) {
  return request({
    url: '/agent_oplog',
    method: 'get',
    params: { page, page_size }
  })
}
