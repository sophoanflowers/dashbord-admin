import request from '@/utils/request'

export function fetchAdmin() {
  return request({
    url: '/admin_user',
    method: 'get'
  })
}

export function fetchAdminDetail(id) {
  return request({
    url: '/admin_user',
    method: 'get',
    params: { id }
  })
}

export function addAdmin(username, password, realname, roles) {
  return request({
    url: '/admin_user/add',
    method: 'post',
    params: { username, password, realname, roles }
  })
}

export function editAdmin(username, password, realname, roles) {
  return request({
    url: '/admin_user/edit',
    method: 'post',
    params: { username, password, realname, roles }
  })
}

export function banAdmin(ids) {
  return request({
    url: '/admin_user/disable',
    method: 'post',
    params: { ids }
  })
}

export function unbanAdmin(ids) {
  return request({
    url: '/admin_user/enable',
    method: 'post',
    params: { ids }
  })
}

export function deleteAdmin(ids) {
  return request({
    url: '/admin_user/delete',
    method: 'post',
    params: { ids }
  })
}
