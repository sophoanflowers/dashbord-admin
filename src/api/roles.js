import request from '@/utils/request'

export function fetchRoleList(page, page_size, username, realname) {
  return request({
    url: '/admin_user',
    method: 'get',
    params: { page, page_size, username, realname }
  })
}

export function fetchRoleDetail(id) {
  return request({
    url: '/admin_user/detail',
    method: 'get',
    params: { id }
  })
}

export function listRole() {
  return request({
    url: '/role',
    method: 'get'
  })
}

export function addRole(username, password, realname, roles) {
  return request({
    url: '/admin_user/add',
    method: 'post',
    params: { username, password, realname, roles }
  })
}

export function editRole(username, password, realname, roles) {
  return request({
    url: '/admin_user/edit',
    method: 'post',
    params: { username, password, realname, roles }
  })
}

export function disableRole(ids) {
  return request({
    url: '/admin_user/disable',
    method: 'post',
    params: { ids }
  })
}

export function enableRole(ids) {
  return request({
    url: '/admin_user/enable',
    method: 'post',
    params: { ids }
  })
}

export function deleteUser(ids) {
  return request({
    url: '/admin_user/delete',
    method: 'post',
    params: { ids }
  })
}
