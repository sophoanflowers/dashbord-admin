import request from '@/utils/request'

export function fetchUserWithdrawals(page, page_size, uid, agent_id, date_start, date_end) {
  return request({
    url: '/user_balance_data',
    method: 'get',
    params: { page, page_size, uid, agent_id, date_start, date_end }
  })
}
export function fetchChannelWithdrawals(page, page_size, date_start, date_end, agent_id, is_saturate) {
  return request({
    url: '/agent_balance_data',
    method: 'get',
    params: { page, page_size, date_start, date_end, agent_id, is_saturate }
  })
}
