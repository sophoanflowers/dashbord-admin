import request from '@/utils/request'

export function fetchMarquee(page, page_size) {
  return request({
    url: '/marquee',
    method: 'get',
    params: { page, page_size }
  })
}

export function fetchMarqueeDetail(id) {
  return request({
    url: '/marquee/detail',
    method: 'get',
    params: { id }
  })
}

export function addMarquee(content, status) {
  return request({
    url: '/marquee/add',
    method: 'post',
    params: { content, status }
  })
}

export function editMarquee(id, title, content, status) {
  return request({
    url: '/marquee/edit',
    method: 'post',
    params: { id, title, content, status }
  })
}

export function enableMarquee(ids) {
  return request({
    url: '/marquee/enable',
    method: 'post',
    params: { ids }
  })
}

export function disableMarquee(ids) {
  return request({
    url: '/marquee/disable',
    method: 'post',
    params: { ids }
  })
}

export function deleteMarquee(id) {
  return request({
    url: '/marquee/delete',
    method: 'post',
    params: { id }
  })
}
