import request from '@/utils/request'

export function fetchRoom(page, page_size, title, status) {
  return request({
    url: 'room',
    method: 'get',
    params: { page, page_size, title, status }
  })
}

export function fetchRoomDetail(id) {
  return request({
    url: 'room/detail',
    method: 'get',
    params: { id }
  })
}

export function addRoom(name, cover_img, video_url, status) {
  return request({
    url: 'room/add',
    method: 'post',
    params: { name, cover_img, video_url, status }
  })
}

export function editRoom(id, name, cover_img, video_url, status) {
  return request({
    url: 'room/edit',
    method: 'post',
    params: { id, name, cover_img, video_url, status }
  })
}

export function deleteRoom(id) {
  return request({
    url: 'room/delete',
    method: 'post',
    params: { id }
  })
}

export function enableRoom(ids) {
  return request({
    url: 'room/enable',
    method: 'post',
    params: { ids }
  })
}

export function disableRoom(ids) {
  return request({
    url: 'room/disable',
    method: 'post',
    params: { ids }
  })
}
