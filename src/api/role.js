import request from '@/utils/request'

export function getRoutes() {
  return request({
    url: '/permission_list',
    method: 'get'
  })
}

export function getRoles() {
  return request({
    url: '/role',
    method: 'get'
  })
}

export function getRoleDetail(id) {
  return request({
    url: '/role/detail',
    method: 'get',
    params: { id }
  })
}

export function addRole(name, permissions) {
  return request({
    url: '/role/add',
    method: 'post',
    data: { name, permissions }
  })
}

export function updateRole(id, name, permissions) {
  return request({
    url: '/role/edit',
    method: 'post',
    data: { id, name, permissions }
  })
}

export function deleteRole(id) {
  return request({
    url: '/role/delete',
    method: 'post',
    data: { id }
  })
}
