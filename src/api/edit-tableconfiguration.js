import request from '@/utils/request'

export function editMinOrder(min_order) {
  return request({
    url: 'config_table/edit',
    method: 'post',
    params: { min_order }
  })
}

export function editSingleBetLimit(single_bet_limit) {
  return request({
    url: 'config_table/edit',
    method: 'post',
    params: { single_bet_limit }
  })
}

export function editBankerPayLimit(banker_pay_limit) {
  return request({
    url: 'config_table/edit',
    method: 'post',
    params: { banker_pay_limit }
  })
}

export function editBuildTime(build_time) {
  return request({
    url: 'config_table/edit',
    method: 'post',
    params: { build_time }
  })
}

export function editPlatformCommission(platform_commission) {
  return request({
    url: 'config_table/edit',
    method: 'post',
    params: { platform_commission }
  })
}

export function editTableOwnerCommission(table_owner_commission) {
  return request({
    url: 'config_table/edit',
    method: 'post',
    params: { table_owner_commission }
  })
}
