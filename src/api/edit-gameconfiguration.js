import request from '@/utils/request'

export function editBetTime(bet_times) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { bet_times }
  })
}

export function editCardHead(card_head) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { card_head }
  })
}

export function editBetCountdownTime(bet_countdown_time) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { bet_countdown_time }
  })
}

export function editCardHeadTime(card_head_time) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { card_head_time }
  })
}

export function editDiceTime(dice_time) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { dice_time }
  })
}

export function editOpenCardTime(open_card_time) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { open_card_time }
  })
}

export function editSZMinMPL(sz_min_mpl) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { sz_min_mpl }
  })
}

export function editGZMinMPL(gz_min_mpl) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { gz_min_mpl }
  })
}

export function editBZMinMPL(bz_min_mpl) {
  return request({
    url: 'config_game/edit',
    method: 'post',
    params: { bz_min_mpl }
  })
}
