import request from '@/utils/request'

export function fetchUserActive(page, page_size, agent_id, date_start, date_end) {
  return request({
    url: 'member_active_list',
    method: 'get',
    params: { page, page_size, agent_id, date_start, date_end }
  })
}

export function fetchUserRetention(page, page_size, agent_id, date_start, date_end) {
  return request({
    url: 'member_retention_list',
    method: 'get',
    params: { page, page_size, agent_id, date_start, date_end }
  })
}

export function fetchUserOnline(page, page_size, agent_id, date_start, date_end) {
  return request({
    url: 'member_online_list',
    method: 'get',
    params: { page, page_size, agent_id, date_start, date_end }
  })
}
