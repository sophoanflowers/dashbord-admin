import request from '@/utils/request'

export function getCaptcha() {
  return request({
    url: 'get_captcha',
    method: 'get'
  })
}
export function reloadCaptcha() {
  return request({
    url: 'reload_captcha',
    method: 'get'
  })
}
