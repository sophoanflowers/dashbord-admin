import request from '@/utils/request'

export function fetchChannels(page, page_size) {
  return request({
    url: '/agent',
    method: 'get',
    params: { page, page_size }
  })
}

export function editChannels(id, desc, realname, agent_balance, password) {
  return request({
    url: '/agent/edit',
    method: 'post',
    params: { id, password, desc, realname, agent_balance }
  })
}
export function addChannels(data) {
  return request({
    url: '/agent/add',
    method: 'post',
    params: { username: data.name, password: data.password, realname: data.realname, agent_balance: data.agent_balance, status: data.status, desc: data.desc }
  })
}
export function enable(ids) {
  return request({
    url: '/agent/enable',
    method: 'post',
    params: { ids }
  })
}
export function disable(ids) {
  return request({
    url: '/agent/disable',
    method: 'post',
    params: { ids }
  })
}
