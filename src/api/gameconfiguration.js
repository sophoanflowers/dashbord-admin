import request from '@/utils/request'

export function fetchGameConfiguration() {
  return request({
    url: 'config_game',
    method: 'get'
  })
}

export function fetchTableConfiguration() {
  return request({
    url: 'config_table',
    method: 'get'
  })
}

export function fetchMaintenanceConfiguration() {
  return request({
    url: 'maintenance',
    method: 'get'
  })
}

export function editMaintenanceConfiguration(sys_in_maintenance, maintenance_title, maintenance_content) {
  return request({
    url: 'maintenance/edit',
    method: 'post',
    params: { sys_in_maintenance, maintenance_title, maintenance_content }
  })
}
