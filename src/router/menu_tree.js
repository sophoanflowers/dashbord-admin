import Layout from '@/layout'

export function parse_menu(data) {
  const result = getPage(data)
  result.push({ path: '*', redirect: '/404', hidden: true })
  return result
}

/**
 * 构建菜单
 *
 * @param allList
 * @returns {[]}
 */
function getPage(allList) {
//   const tempArr = allList
  // 构建的菜单数据
  const menuList = []
  // 获取所有的父级菜单
  allList.forEach((menu, index) => {
    // 一级菜单parentId == 0
    if (menu.parent_id === 0) {
      // 判断该菜单是目录还是菜单 0=目录，1=菜单
      let temp = {}
      temp = {
        menuId: menu.id,
        path: menu.url.split('/')[1],
        redirect: menu.url,
        name: menu.name,
        component: Layout,
        meta: { title: menu.name, icon: menu.icon ? menu.icon : 'form' },
        children: []
      }
      menuList.push(temp)
    }
  })
  menuList.forEach((menu, index) => {
    menu.children = menu.children.concat(getChild(menu.menuId, allList))
  })

  return menuList
}

/**
 *  构建子菜单数据
 *
 * @param menuId
 * @param menuArr
 * @returns {[]}
 */
function getChild(menuId, allList) {
  const chileArr = []
  allList.forEach((menu, index) => {
    // 遍历所有节点，将父菜单id与传过来的id比较
    if (menu.parent_id !== 0) {
      if (menu.parent_id === menuId && menu.url !== '') {
        let temp = {}
        temp = {
          path: menu.url,
          name: menu.name,
          hidden: menu.is_show !== 1,
          component: resolve => require([`@/views${menu.url}/index`], resolve),
          meta: { title: menu.name }
        }
        chileArr.push(temp)
      }
    }
  })
  return chileArr
}

